<?php
/**
 * Created by PhpStorm.
 * User: SakuyaIwahashi
 * Date: 2017/08/21
 * Time: 13:40
 */

//model読み込み
include 'register_model.php';

?>

<?php include "header.php"; ?>
    <div class="text-center">
        <h1>新規会員登録</h1>

    <form class="form-horizontal" action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
        <div class="form-group">
            <label for="inputUsername" class="col-sm-4 control-label">ユーザーネーム</label>
            <div class="col-sm-4">
                <input type="text" name="user_name" value="<?= $user_name?>" class="form-control" id="inputUsername" placeholder="ユーザーネーム" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="col-sm-4 control-label">パスワード</label>
            <div class="col-sm-4">
                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="パスワード" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputMail" class="col-sm-4 control-label">メールアドレス</label>
            <div class="col-sm-4">
                <input type="email" name="mail" value="<?= $mail?>" class="form-control" id="inputMail" placeholder="****@****" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputMailConfirm" class="col-sm-4 control-label">メールアドレス（確認）</label>
            <div class="col-sm-4">
                <input type="email" name="mail_confirm" value="<?= $mail_confirm?>" class="form-control" id="inputMail" placeholder="****@****" required>
            </div>
        </div>

        <div >
            <input type="submit" name="" value="新規登録" class="btn btn-primary" ><a href="login.php"></a>
        </div>
    </form>
    </div>
<?php include "footer.php"; ?>