<?php
    //セッションスタート
    session_start();

    //セッション破壊
    session_destroy();

    //トップページへ
    header("Location: ./login.php");