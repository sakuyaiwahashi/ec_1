<?php
    session_start();
    //変数読み込み
    include "function.php";

    //セッション変数受け渡し
    //ログインフラグ：ＯＮ
    if (isset($_SESSION["user_name"])) {
        $user_name = $_SESSION["user_name"];
        $login_flg = 1;
    }

    //$user_nameにgetで受け取ったuser_nameを代入
    $user_name_get = $_GET['user_name_get'];

    //データベース読み込み、新規ＰＤＯ作成
    $db = new PDO("mysql:host=localhost;dbname=trashtalk", "root", "");

    //postsテーブルのuse_nameと、stoke_$user_name_getが一致するレコードを読み出す
    $sql = "SELECT * FROM posts WHERE user_name = '${user_name_get}'";

    $result = $db -> query($sql);
    $rows = $result -> fetchall(PDO::FETCH_ASSOC);

    //投稿時間で並べ替え
    $sort = array();
    foreach ($rows as $key => $value) {
        $sort[$key] = $value['post_date'];
    }
    array_multisort($sort, SORT_DESC, $rows);

    // 個別のデータを読み込む(users, stoke, stoker, posts)
    $sql_posts = "SELECT * FROM users WHERE user_name = '${user_name_get}'";
    $result_posts = $db -> query($sql_posts);
    $rows_posts = $result_posts -> fetchall(PDO::FETCH_ASSOC);

    $sql_stoke = "SELECT * FROM stoke_$user_name_get";
    $result_stoke = $db -> query($sql_stoke);
    $rows_stoke = $result_stoke -> fetchall(PDO::FETCH_ASSOC);

    $sql_stoker = "SELECT * FROM stoker_$user_name_get";
    $result_stoker = $db -> query($sql_stoker);
    $rows_stoker = $result_stoker -> fetchall(PDO::FETCH_ASSOC);
