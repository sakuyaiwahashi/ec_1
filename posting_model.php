<?php
    //セッションスタート
    session_start();

    //変数読み込み
    include 'function.php';

    //セッション変数受け渡し
    //セッション変数：ユーザーＩＤ
    if (isset($_SESSION["user_name"])) {
        $user_name = $_SESSION["user_name"];
        $login_flg = 1;
    }

    //POSTで受け取った場合の処理
    if($_SERVER["REQUEST_METHOD"] === "POST") {
        //POST受け取り
        $post_text = $_POST['post_text'];
        $post_image = $_FILES['post_image']['name'];

        //データベース読み込み、新規ＰＤＯ作製
        $db = new PDO("mysql:host=localhost;dbname=trashtalk", "root", "");

        //UNIX TIMESTAMPを取得
        $timestamp = time() ;

        //date()で日時を取得して$post_dateへ代入
        $post_date = date( "Y/m/d/H:i:s" , $timestamp ) ;

        //postsテーブルへ、$user_name, $post_text, $post_image, $post_dateを挿入
        $sql = "INSERT INTO posts (user_name, post_text, post_image, post_date) VALUES ('${user_name}', '${post_text}', '${post_image}', '${post_date}')";
        $result = $db->query($sql);

        //アップロードファイル保存
        if (strncmp(strtoupper(PHP_OS), "WIN", 3) == 0) {
                // シフトＪＩＳ(SJIS)・ＯＳ：ＭＳ－Ｗｉｎｄｏｗｓ
                // 保存パス・ファイル名：全角変換 ＵＴＦ－８ ---> シフトＪＩＳ
                $move_path = $image_folder . mb_convert_encoding($post_image, "SJIS", "UTF-8");
            } else {
                // ＵＴＦ－８・ＯＳ：Ｌｉｎuｘ、ＵＮＩＸ等（Ｗｉｎｄｏｗｓ以外）
                $move_path = $image_folder . $post_image;
            }
        // アップロードファイルの移動：一時ディレクトリ・ファイル ---> 保存フォルダ・ファイル
        move_uploaded_file($_FILES["post_image"]["tmp_name"], $move_path);

        //top.phpへ
        header("Location: ./top.php");

    }
?>
