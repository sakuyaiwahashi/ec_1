<?php
//セッションスタート
session_start();

// 変数読み込み
include "function.php";

//セッション変数受け渡し
//セッション変数：ユーザーＩＤ
if (isset($_SESSION["user_name"])) {
    $user_name = $_SESSION["user_name"];
    $login_flg = 1;
}

//ログインしてるかどうか(してない場合はlogin.phpへ)
if ($login_flg == 0) {
    header("Location: ./login.php");
    exit;
}

//データベース読み込み、新規ＰＤＯ作成
$db = new PDO("mysql:host=localhost;dbname=trashtalk", "root", "");

//postsテーブルの全要素を取得
$sql = "SELECT * FROM posts";
$result = $db -> query($sql);
$rows = $result -> fetchall(PDO::FETCH_ASSOC);

//投稿時間で並べ替え
$sort = array();
foreach ($rows as $key => $value) {
    $sort[$key] = $value['post_date'];
}
array_multisort($sort, SORT_DESC, $rows);

//以下、全コメントの取得
$sql_comment = "SELECT * FROM COMMENTS";
$result_comment = $db -> query($sql_comment);
$rows_comment = $result_comment -> fetchall(PDO::FETCH_ASSOC);
