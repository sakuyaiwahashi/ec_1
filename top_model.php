<?php
    //セッションスタート
    session_start();

    // 変数読み込み
    include "function.php";

    //セッション変数受け渡し
    //セッション変数：ユーザーＩＤ
    if (isset($_SESSION["user_name"])) {
        $user_name = $_SESSION["user_name"];
        $login_flg = 1;
    }

    //ログインしてるかどうか(してない場合はlogin.phpへ)
    if ($login_flg == 0) {
        header("Location: ./login.php");
        exit;
    }

    //フォローしてる人の投稿を時間毎に表示
    
