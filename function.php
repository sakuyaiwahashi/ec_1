<?php
    // register.php
    $err_msg          = []; //エラーメッセージ
    $user_name        = ''; //ユーザーネーム
    $password         = ''; //パスワード
    $mail             = ''; //メールアドレス
    $mail_confirm     = ''; //メールアドレス（確認）

    // posting.php
    $post_text        = ''; //投稿内容
    $post_image       = ''; //アップロードされたファイル（画像）
    $post_date        = ''; //投稿日時
    $move_path        = ''; //保存場所
    $image_folder     = './images/'; //imagesフォルダ

    //flg
    $p_flg            = 0;  //POSTフラグ
    $login_flg        = 0;  //ログインフラグ
