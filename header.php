<?php
/**
 * Created by PhpStorm.
 * User: yamamototatsuya
 * Date: 2017/08/13
 * Time: 10:27
 */
?>


<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/reset.css">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./sass/style.css">
    <title>Document</title>
</head>
<body>
<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarEexample6">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="top.php">
                    Trash Talk
                    <img src="./images/bird.jpeg" alt="">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="navbarEexample6">
                <ul class="nav navbar-nav">
                    <li><a href="#"></a></li>
                    <?php if ($login_flg == 0) :?>
                    <li><a href="login.php">login</a></li>
                    <?php else:?>
                    <li><a href="personal.php">Profile</a></li>
                    <li><a href="posting.php">post</a></li>
                    <li><a href="logout.php">logout</a></li>
                </ul>
                <p class="navbar-text">ようこそ <a href="#" class="navbar-link"><?=$user_name?></a> さん。</p>
                    <?php endif; ?>
            </div>
        </div>
    </nav
</header>